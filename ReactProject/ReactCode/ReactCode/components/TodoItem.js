import React, { useState } from "react";
import { Image, View, Text, StyleSheet, Switch, TouchableOpacity } from 'react-native';
import { SIZES, FONTS, COLORS, SHADOW } from "../constants/constants.js"

export default function TodoItem(props) {
   
    //console.log(props.item.id);
    
    const [done, setDone] = useState(props.item.done);
    
    if(done!=props.item.done)
     setDone(props.item.done);
    
    const toggleSwitch = () => setDone(currentState => !currentState);
    const deleteItem = () => props.deleteTodo(props.item.id);
    const changedoneOnItem = () => props.onPressed(props.item,1);
    const changedoneOnItem2 = () => props.onPressed(props.item,2);
    
    return (
        
        <View style={styles.view}>
             <TouchableOpacity onPress={!done ? changedoneOnItem : changedoneOnItem2}>
             <Switch style={styles.checkbox} value={done} onValueChange={toggleSwitch} />
            </TouchableOpacity>
            <Text style={[styles.text, { textDecorationLine: done ? 'line-through' : 'none' }]}>{props.item.content}</Text>
            <TouchableOpacity onPress={  deleteItem  }>
                <Image source={require('../assets/trash-can-outline.png')} style={{ marginLeft: 15 ,height: 24, width: 24 }} />
            </TouchableOpacity>
            

            

        </View>
        
        
    )
   
}

function Candeletefunction(props) {
    if (props.candelete) {    
    return (
        <div > delted successfully {props.deleteTodo(props.id)} </div>
    );}
    else 
    return null;
  }

const styles = StyleSheet.create({
    content: {
        flexDirection: 'row'
    },
    text_item: {
        marginLeft: 10,
        width: 150
    },
    view: {
        ...SHADOW,
        width: '100%',
        paddingVertical: 10,
        paddingHorizontal: SIZES.padding,
        borderRadius: SIZES.borderRadius,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: COLORS.secondary,
        marginBottom: 15
    },
    text: {
        ...FONTS.h2_semiBold,
        color: COLORS.primary
    },
    checkbox: {
        marginRight: 15
    }
})