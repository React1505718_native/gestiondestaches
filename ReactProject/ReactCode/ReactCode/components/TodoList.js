import React ,{useEffect, useState, useContext}  from "react";
import { StyleSheet, View, TextInput, Button, TouchableOpacity,Text,  StatusBar, Platform,  FlatList } from "react-native"
import todoData from '../Helpers/todoData';
import TodoItem from './TodoItem';
import { SIZES, FONTS, COLORS, SHADOW } from "../constants/constants.js"
import { getTasks,deleteTasks,updateTasks,createTask } from '../API/todoAPI'
import { clickProps } from "react-native-web/dist/cjs/modules/forwardedProps";
import {UsernameContext, TokenContext} from "../Context/Context";

export default function TodoList(props){
    
    
    const  [todoDataBase, setTodos]=  useState([]);
    const [count, setCount] = useState(0);
    
    const [token, setToken] = useContext(TokenContext);
    const [user, setUser] = useContext(UsernameContext);
     
    useEffect(() => {
        getTasks(user, token).then((data) => {
            const newList = data.map((item) => ({id: item.id, content: item.title, done: item.done}));
            setTodos(newList);
            setDatabase(newList);
        });
    }, []);
    

 

    const onPressed = (itm,is) => {
       const id= itm.id;
       const done = !itm.done;
       const content =itm.content;
       

      

      //deleteTodo(id);

      /*deleteTasks(user, id, token).then((data) => {
        const newTodos = todoDataBase.filter((item) => item.id != id);
        setTodos(newTodos);
    });
      createTask(user, newTodoText, token).then((data) => {
           
        const new_id = id;
        const new_item = {id:new_id, content:content, done:done}
        const newTodos = todoDataBase.filter((item) => item.id != id);
        const myArr =  [...newTodos, new_item] 
        setTodos(myArr)
        setDatabase(myArr)    
        });*/

     /* updateTasks(user, id, done, token).then((data) => {
            const newTodos = data.filter(item => item.id != id)
            itm.done = !itm.done
            const myArr =  [...newTodos,itm]
            setTodos(myArr)
            setDatabase(myArr)
        });*/
       
       
        const newTodos = todoDataBase.filter(item => item.id != itm.id)
        itm.done =!itm.done
        const myArr =  [...newTodos,itm] 
        setDatabase(myArr)
        if(is==1)
        setCount(count + 1)
        if(is==2)
        setCount(count - 1)
    };



    const deleteTodo = (id) => {
        
        deleteTasks(user, id, token).then((data) => {
            const newTodos = todoDataBase.filter((item) => item.id != id);
            setTodos(newTodos);
            setCount(newTodos.filter(item => item.done).length)
            setDatabase(newTodos)
        });
        //const newTodos = todoDataBase.filter(item => item.id != id)
        //console.log(newTodos)
        //setTodos(newList);
        //setCount(newTodos.filter(item => item.done).length)
        //setDatabase(newTodos)

    };

    const [newTodoText, setNewTodoText] = useState('');
    
    
    const addNewTodo = () => {
        createTask(user, newTodoText, token).then((data) => {
           
        const new_id = todoDataBase.length == 0 ? 1 : todoDataBase.length+1;
        const new_item = {id:new_id, content:newTodoText, done:false}
        const myArr =  [...todoDataBase, new_item] 
        setTodos(myArr)
        setNewTodoText('')
        setDatabase(myArr)    
        });
       
       /* const new_id = todoDataBase.length == 0 ? 1 : Math.max(...todoDataBase.map(item => item.id))+1;
        const new_item = {id:new_id, content:newTodoText, done:false}
        const myArr =  [...todoDataBase, new_item] 
        setTodos(myArr)
        setNewTodoText('')
        setDatabase(myArr)*/


    }
    
    const  [dbtoshow, setDatabase]=  useState(todoDataBase);
    
    const checkAll = () => {
        const db1 = todoDataBase.map(item => item = {id: item.id, content: item.content, done: true })
        const new4db = db1.filter((item) => item.done);
        setDatabase(new4db)
        setCount(todoDataBase.length)
        setTodos(new4db)      
        
    }
    
    const UncheckAll = () => {
        const db = todoDataBase.map(item => item = {id: item.id, content: item.content, done: false })
        const newdb = db.filter((item) => !item.done);
        setDatabase(newdb)
        setCount(0)
        setTodos(newdb)
           
    }
    
    const afficherTout = () => { setDatabase(todoDataBase)}

    const afficherEnCours = () => {  
        const new_db = todoDataBase.filter((item) => item.done);
        setDatabase(new_db) }

    const afficherNonResolu = () => {  const new_db2 = todoDataBase.filter((item) => !item.done);
        setDatabase(new_db2) }
    
 


    return (
        <View style={styles.container}>
            <View style={styles.progressBar}>
              <div style={{height :40 ,borderRadius : 30,
            backgroundColor : COLORS.accent, width : count==0 ? 0 : (count/todoDataBase.length)*330 }}>    </div>
            </View>
            
            
            <Text style={{ ...FONTS.h1_semiBold, color: COLORS.secondary, marginBottom: 15 }}> nombre des taches realisés : {count}</Text>
            
           
           <View style={ {flexDirection: "row"}} >
            <TouchableOpacity
                style={styles.mybtn}
                onPress={checkAll}>
                <Text style={{ fontSize: 18, color: COLORS.secondary }}>CheckAll</Text>
            </TouchableOpacity>
            
            <TouchableOpacity
                style={styles.mybtn}
                onPress={UncheckAll}>
                <Text style={{ fontSize: 18, color: COLORS.secondary }}>UnCheckAll</Text>
            </TouchableOpacity>
            
            <TouchableOpacity
                style={styles.mybtn}
                onPress={afficherTout}>
                <Text style={{ fontSize: 18, color: COLORS.secondary }}>Show All</Text>
            </TouchableOpacity>
            </View>
            
            <View style={ {flexDirection: "row", marginBottom :10}} >
            <TouchableOpacity
                style={{...SHADOW,
                    backgroundColor: COLORS.accent,
                    height: 47,
                    width: 92,
                    borderRadius: 100,
                marginRight : 35,
                marginLeft : 50}}
                onPress={afficherEnCours}>
                <Text style={{ fontSize: 17, color: COLORS.secondary }}>Todos in Process</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={{...SHADOW,
                    backgroundColor: COLORS.accent,
                    height: 47,
                    width: 92,
                    borderRadius: 100,
                }}
                onPress={afficherNonResolu}>
                <Text style={{ fontSize: 17, color: COLORS.secondary }}>Todos Not done yet</Text>
            </TouchableOpacity>
            </View>
            <FlatList
                style={{ paddingLeft: 10 , flex :1 }}
                data=  {dbtoshow}
                renderItem={({item}) => <TodoItem item={item} deleteTodo={deleteTodo} onPressed={onPressed} count = {count}/>} 
                />
            
            <View  style={styles.textBoxWrapper} >
                <TextInput
                    style={styles.textInput}
                    onChangeText={setNewTodoText}
                    placeholder='saisir ici un nouvel item'
                    onSubmitEditing={addNewTodo}
                    value={newTodoText} 
                />
                <TouchableOpacity
                style={styles.btn}
                onPress={addNewTodo}>
                <Text style={{ fontSize: 44, color: COLORS.secondary }}>+</Text>
               </TouchableOpacity>
               
            </View>
           
        </View>
    )
}


function refreshPage() {
    window.location.reload(false);
  }

  

  const styles = StyleSheet.create({
      container: {
          paddingTop: Platform.OS === "ios" ? 40 : StatusBar.currentHeight + 20,
          flex: 1,
          backgroundColor: COLORS.primary,
          padding: SIZES.padding
      },
      textBoxWrapper: {
          width: "100%",
          position: "absolute",
          bottom: 0,
          left: 0,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "space-between",
          padding: SIZES.padding
      },
      textInput: {
          ...SHADOW,
          borderRadius: SIZES.textBoxRadius,
          backgroundColor: COLORS.secondary,
          height: 42,
          paddingLeft: 15,
          width: "90%",
          color: COLORS.primary,
          marginRight: 15,
          ...FONTS.h2_semiBold,
      },
      btn: {
          ...SHADOW,
          backgroundColor: COLORS.accent,
          height: 42,
          width: 42,
          borderRadius: 100,
          alignItems: "center",
          justifyContent: "center",
          
      },

      mybtn :{
        ...SHADOW,
        backgroundColor: COLORS.accent,
        height: 32,
        width: 82,
        borderRadius: 100,
        alignItems: "center",
        justifyContent: "center",
        marginRight:30,
        marginBottom:10,
      },
      progressBar: {
        border : 4 ,
        borderRadius : 30,
        backgroundColor : COLORS.secondary,
        height : 40,
        width: 330,
        marginLeft:7,
        marginRight:0

    }
  })
  
  