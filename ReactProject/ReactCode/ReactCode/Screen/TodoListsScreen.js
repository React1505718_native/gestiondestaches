import React from 'react'
import { View, Text, Button, StyleSheet} from 'react-native'
import TodoList from '../components/TodoList';


export default function TodoLists(){
    return (
        <View style={styles.container}>
              <Text>Liste des TodoLists</Text>
              <TodoList />
    </View>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });