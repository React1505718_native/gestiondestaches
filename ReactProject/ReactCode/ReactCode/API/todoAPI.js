const API_URL = 'http://localhost:4000/'

const SIGN_IN =
  'mutation($username:String!, $password:String!){signIn(username:$username, password:$password)}'

const SIGN_UP =
  'mutation($username:String!, $password:String!){signUp(username:$username, password:$password)}'

const TASKS = ' query($username: String!) {tasks(where: { owner: { username: $username } }) {id title done}}'

const CREATE_TASK =
  `mutation($username: String!, $title: String!) {
    createTasks(input: { title: $title, done: false, owner: {connect : {where : {username : $username}}} }) {
      tasks {
        id
        title
        done
      }
    }
  }
  `

const DELETE_TASKS =
  `mutation($username: String!, $id: ID!) {
    deleteTasks(where: { AND: { owner: { username: $username }, id: $id } }) {
      nodesDeleted
    }
  }`

const UPDATE_TASKS=
`mutation($username: String!, $id: ID!,$done:Boolean) {
  updateTasks(where: { AND: { owner: { username: $username }, id: $id } update:{done:$done}}) {
    tasks {
      id
      title
      done
    }
  }
}`
  

export function getTasks(username, token){
  return fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    },
    body: JSON.stringify({
      query: TASKS,
      variables: {
        username: username
      }
    })
  })
    .then(response => {
      return response.json()
    })
    .then(jsonResponse => {
      if (jsonResponse.errors != null) {
        throw jsonResponse.errors[0]
      }
      return jsonResponse.data.tasks
    })
    .catch(error => {
      throw error
    })}

    export function deleteTasks(username, id, token){
      return fetch(API_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
          query: DELETE_TASKS,
          variables: {
            username: username,
            id: id
          }
        })
      })
        .then(response => {
          return response.json()
        })
        .then(jsonResponse => {
          if (jsonResponse.errors != null) {
            throw jsonResponse.errors[0]
          }
          return jsonResponse.data
        })
        .catch(error => {
          throw error
        })
    }
    

    export function updateTasks(username, id, done, token){
      return fetch(API_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
          query: UPDATE_TASKS,
          variables: {
            username: username,
            id: id,
            done: done
          }
        })
      })
        .then(response => {
          return response.json()
        }) 
        .then(jsonResponse => {
          if (jsonResponse.errors != null) {
            throw jsonResponse.errors[0]
          }
          return jsonResponse.data.updateTasks.tasks[0]
        })
        .catch(error => {
          throw error
        })
    }
    

    export function createTask(username, task, token){
      return fetch(API_URL, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token,
        },
        body: JSON.stringify({
          query: CREATE_TASK,
          variables: {
            username: username,
            title: task
          }
        })
      })
        .then(response => {
          return response.json()
        }
        )
        .then(jsonResponse => {
          if (jsonResponse.errors != null) {
            throw jsonResponse.errors[0]
          }
          return jsonResponse.data.createTasks.tasks[0]
        })
        .catch(error => {
          throw error
        })
    }
    


    
export function signIn (username, password) {
  return fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query: SIGN_IN,
      variables: {
        username: username,
        password: password
      }
    })
  })
    .then(response => {
      return response.json()
    })
    .then(jsonResponse => {
      if (jsonResponse.errors != null) {
        throw jsonResponse.errors[0]
      }
      return jsonResponse.data.signIn
    })
    .catch(error => {
      throw error
    })
}

export function signUp (username, password) {
  return fetch(API_URL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      query: SIGN_UP,
      variables: {
        username: username,
        password: password
      }
    })
  })
    .then(response => {
      return response.json()
    })
    .then(jsonResponse => {
      if (jsonResponse.errors != null) {
        throw jsonResponse.errors[0]
      }
      return jsonResponse.data.signUp
    })
    .catch(error => {
      throw error
    })
}
